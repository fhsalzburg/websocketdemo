<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Message Sending</h1>
	<form>
		<input id="textMessage" type="text" />
		<input onclick="sendMessage();" value="Send Message" type="button" />
	</form>
	<br/>
	<textarea id="messages" rows="10" cols="30"></textarea>
	
	<script>
		var socket = new WebSocket("ws://localhost:8080/websocketdemo/websocketendpoint");		
		var messages = document.getElementById("messages");
		var textMessage = document.getElementById("textMessage");
		
		socket.onopen = function(message) { processOpen(message); }
		socket.onmessage = function(message) { processMessage(message); }
		socket.onclose = function(message) { processClose(message); }
		socket.onerror = function(message) { processError(message); }
		
		function processOpen(message) {
			messages.value += "Server Connect ... " + "\n";
		}
		function processMessage(message) {
			messages.value += "Receive from Server ==> " + message.data + "\n";
		}
		function sendMessage(message) {
			if (textMessage.value == "close") {				
				socket.close();
			} else {
				socket.send(textMessage.value);
				messages.value += "Send to Server ==> " + textMessage.value + "\n";
				textMessage.value = "";
			}
			
		}
		function processClose(message) {
			socket.send("client disconnected");
			messages.value += "Server Disconnect ... " + message.data + "\n";
		}
		function processError(message) {
			messages.value += "error ...\n";
		}
		
	</script>
</body>
</html>