package at.fhs.its.nos.websocketdemo.simpletext;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocketendpoint")
public class SocketServerEndpoint {
	@OnOpen
	public void handleOpen() {
		System.out.println("connected ...");
	}
	
	@OnMessage
	public String handleMessage(String message) {
		System.out.println("received from client: " + message);
		String replyMessage = "echo: " + message;
		
		System.out.println(replyMessage);
		
		return replyMessage;
	}
	
	@OnClose
	public void handleClose() {
		System.out.println("disconnected ...");
	}

	@OnError
	public void handleError(Throwable t) {
		t.printStackTrace();
	}
}